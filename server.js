const express = require('express')
const app = express()
const port = 3000


app.get('/', (req, res) => {
	res.sendFile('index.html', {root: __dirname })
})
app.get('/main', (req,res) => { 
	res.sendFile('main.js', {root: __dirname})
})

app.use('/drawings', express.static('drawings'))

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`)
})
