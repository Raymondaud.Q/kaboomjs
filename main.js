/*
    1. Need To Check out of range bullet and remove them
    2. Refactor for a second Player
    3. Gameplay code ( Put the fire or stop it )
    4. Create the background & sprites

 Optional :

    - Welcome Panel
    - Config keyboard
    - Sound
*/

// initialize kaboom context
const k = kaboom({
                    global: true,
                    width:window.innerWidth,
                    height:window.innerHeight
                    
                });

loadSprite("humon", "http://localhost:3000/drawings/Humon-1.png");

// add a text of size 32 at position (100, 100)
add([
    text("oh hi", 32),
    pos(100, 100)
]);


const player = add([
    
    //rect(50,50),
    sprite("humon"),
    pos(50, 50),
    origin("center"),
    body({
        // force of .jump()
        jumpForce: 1640,
        // maximum fall velocity
        maxVel: 2400,
        width:100,
        height:100,
    }),
    // color(0,1,1),
    rotate(0)
]);

let angle = player.angle;
player.nbJump = 0;

const platform = addRect( width(), 20, 
    {
        pos: vec2(0, height() - 40),
        solid: true,
    }
);


function spawnBullet(position) { 
    const bullet = add([
        rect(45, 45),
        pos(position),
        origin("center"),
        color(0.5, 0.5, 1),
        body(),
        // strings here means a tag
        "bullet",

    ]);
    bullet.angle = angle.valueOf();
    bullet.speed = 1000;
}


action("bullet", (b) => {
    b.move( Math.sin(b.angle)*b.speed,
            Math.cos(b.angle)*b.speed);
    b.speed -= 1;
    if ( b.grounded() ){
        //destroyAll("bullet");
        destroy(b);
        player.nbJump -=1;
    }
    // remove the bullet if it's out of the scene for performance
});

/*keyPress("down", () => {
    if ( player.nbJump < 10 ) {
        spawnBullet(player.pos.clone());
        player.move(-Math.sin(player.angle)*500,-Math.cos(player.angle)*500);
        player.nbJump += 1;
    }
});*/

keyDown("down", () => {
    spawnBullet(player.pos.clone());
    player.move(-Math.sin(player.angle)*500,-Math.cos(player.angle)*500);
    
});


keyPress("up", () => {
    player.jump(500);
});

keyDown("right", () => {
    if ( player.grounded())
        player.move(500,0);
    else {
        angle -= 0.1;
        angle = ( angle < 0 ? 360 : angle ); 
        player.angle = angle ;
    }

});

keyDown("left", () => {
    if ( player.grounded() )
        player.move(-500,0);
    else {
        angle += 0.1;
        angle = ( angle > 360 ? 0 : angle ); 
        player.angle = angle;
    }
});

// and a "grounded" event
player.on("grounded", () => {
    player.nbJump = 0;
    //gravity(-1600);
});

// or
// addText("oh hi", 32, { pos: vec2(100, 100) });


// if "debug" is enabled, your game gets some special key bindings
// - f1: toggle debug.inspect
// - f2: debug.clearLog()
// - f8: toggle debug.paused
// - f7: decrease debug.timeScale
// - f9: increase debug.timeScale
// - f10: debug.stepFrame()
// see more in the debug section below